using NUnit.Framework;
using System.Net;
using VK_API;

namespace VkAPITests
{
    public class APITests
    {
        Connection connection;
        int Id = 157923312;

        [SetUp]
        public void Setup()
        {
            connection = new Connection();
        }

        [Test]
        public void UrlRespondIsOk()
        {
            connection.SetUrl(Id);

            Assert.That(connection.StatusCode, Is.EqualTo(HttpStatusCode.OK), "Http status code is not OK");
        }

        [Test]
        public void UrlIdConnectionIsCorrect()
        {
            ListOfResponses data = connection.SetUrl(Id);
            Assert.AreEqual(data.response[0].id, Id, "Id is not equal");
        }
        [Test]
        public void OnlineConditionIsNotNegative()
        {
            ListOfResponses data = connection.SetUrl(Id);

            Assert.GreaterOrEqual(data.response[0].online, 0, "Online status is negative!");
        }
    }
}