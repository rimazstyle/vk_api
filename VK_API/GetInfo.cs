﻿using Newtonsoft.Json;
using System;

namespace VK_API
{
    class GetInfo
    {
        Connection connection;
        DataProcessing dataProcessing;
        int userID;

        public GetInfo(int userID)
        {
            connection = new Connection();
            dataProcessing = new DataProcessing();
            this.userID = userID;
        }
        public void IsOnline()
        {
            ListOfResponses data = connection.SetUrl(userID);

            Console.WriteLine($"Пользователь {dataProcessing.IsOnline(data.response[0].online)}");
        }

        public void GetPlatform()
        {
            ListOfResponses data = connection.SetUrl(userID);

            Console.WriteLine($"Платформа {dataProcessing.Platform(data.response[0].last_seen.platform)}");
        }

        public void GetFirstname()
        {
            ListOfResponses data = connection.SetUrl(userID);

            Console.WriteLine(data.response[0].first_name);
        }

        public void GetLastname()
        {
            ListOfResponses data = connection.SetUrl(userID);

            Console.WriteLine(data.response[0].last_name);
        }

        public void GetSeenDate()
        {
            ListOfResponses data = connection.SetUrl(userID);

            DateTimeOffset GetdateTime = DateTimeOffset.FromUnixTimeSeconds(data.response[0].last_seen.time).LocalDateTime;

            string date;

            date = data.response[0].online >= 0 ? GetdateTime.ToString() : dataProcessing.IsOnline(data.response[0].online);

            Console.WriteLine(date);
        }
    }
}
