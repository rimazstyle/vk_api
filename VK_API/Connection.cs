﻿using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace VK_API
{ 
    public class Connection
    {
        public HttpStatusCode StatusCode { get; set; }

        public ListOfResponses SetUrl(int userId)
        {
            string token = "14ddcb2748e68153508149c60fb1f1f6aa79e70c11be8937e7976373da5b6b0e5cf4abbc6f45b81426d69";
            string setUrl = "https://api.vk.com/method/users.get?user_id=" + userId +
                "&fields=bdate,city,sex,online,last_seen,is_closed,can_access_closed,deactivated&v=5.52&access_token=" + token;

            string response = ConnectToVK(setUrl);

            ListOfResponses content = JsonConvert.DeserializeObject<ListOfResponses>(response);

            return content;
        }

        public string ConnectToVK(string url)
        {
            HttpClient httpClient = new HttpClient();
            Task<HttpResponseMessage> httpResponse = httpClient.GetAsync(url);
            HttpResponseMessage httpResponseMessage = httpResponse.Result;
            StatusCode = httpResponseMessage.StatusCode;
            HttpContent content = httpResponseMessage.Content;
            Task<string> responseData = content.ReadAsStringAsync();
            string response = responseData.Result;
            httpClient.Dispose();

            return response;
        }
    }
}
