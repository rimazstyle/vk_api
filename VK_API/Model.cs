﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VK_API
{

    public class City
    {
        public int id { get; set; }
        public string title { get; set; }
    }

    public class Last_seen
    {
        public int time { get; set; }
        public int platform { get; set; }
    }

    public class Response
    {
        public int id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public int sex { get; set; }
        public string bdate { get; set; }
        public City city { get; set; }
        public int online { get; set; }
        public Last_seen last_seen { get; set; }
        public bool is_closed { get; set; }
        public bool can_access_closed { get; set; }
        public string deactivated { get; set; }
    }

    public class ListOfResponses
    {
        public List<Response> response { get; set; }
    }
}
