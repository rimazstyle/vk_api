﻿
namespace VK_API
{
    class DataProcessing
    {
        public string Platform(int platform)
        {
            string typeOfPlatform = null;

            switch (platform)
            {
                case 1:
                    typeOfPlatform = "m.vk.com";
                    break;
                case 2:
                    typeOfPlatform = "iPhone app";
                    break;
                case 3:
                    typeOfPlatform = "iPad app";
                    break;
                case 4:
                    typeOfPlatform = "Android app";
                    break;
                case 5:
                    typeOfPlatform = "Windows Phone app";
                    break;
                case 6:
                    typeOfPlatform = "Windows 8 app";
                    break;
                case 7:
                    typeOfPlatform = "web(vk.com)";
                    break;
                case 8:
                    typeOfPlatform = "VK Mobile";
                    break;
                default:
                    typeOfPlatform = "Устройство не опознано!";
                    break;
            }

            return typeOfPlatform;
        }

        public  string SexType(int sex)
        {
            string sexType = null;

            if (sex == 1)
            {
                sexType = "Женщина";
            }
            else if (sex == 2)
            {
                sexType = "Мужчина";
            }
            else
            {
                sexType = "Пол не указан";
            }

            return sexType;
        }

        public  string IsOnline(int online)
        {
            string isOnline = null;

            isOnline = online >= 1 ? "Онлайн" : "Офлайн";

            return isOnline;
        }
    }
}
